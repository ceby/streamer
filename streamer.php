<?php

$pgpass = split(':', trim(file_get_contents('pgpass')));
$db = new PDO("pgsql:host={$pgpass[0]};dbname={$pgpass[2]}", $pgpass[3], $pgpass[4]);

$coords = array();

$lat = $_GET["lat"];
$lng = $_GET["lng"];
$id = $_GET["id"];

if ($argc >= 3) {
	$lat = $argv[1];
	$lng = $argv[2];
}

if ($id) {
	$lookup_waterway_node = $db->prepare("
		SELECT wn.node_id, w.id AS way_id, wn.sequence_id FROM way_nodes wn
		JOIN ways w ON (w.id = wn.way_id)
		WHERE wn.node_id = :node_id
		AND w.tags -> 'waterway' IN ('stream', 'river', 'ditch', 'canal', 'wadi', 'drain')
		LIMIT 1");
	$lookup_waterway_node->execute(array('node_id' => $id));
	$result = $lookup_waterway_node->fetch();
} else {
	$nearest_waterway_node = $db->prepare("
		SELECT n.id AS node_id, w.id AS way_id, wn.sequence_id FROM nodes n
		JOIN way_nodes wn ON (n.id = wn.node_id)
		JOIN ways w ON (w.id = wn.way_id)
		WHERE w.tags -> 'waterway' IN ('stream', 'river', 'ditch', 'canal', 'wadi', 'drain')
		ORDER BY n.geom <-> ST_SetSRID(ST_Point(:lng, :lat),4326)
		LIMIT 1");
	$nearest_waterway_node->execute(array('lat' => $lat, 'lng' => $lng));
	$result = $nearest_waterway_node->fetch();
}

echo "{\"id\": {$result[0]}, \"linestring\": [";

if ($result) {
	$db->beginTransaction();
	$db->query("
		DO $\$DECLARE
			i integer := 1;
			last_way bigint := {$result[1]};
			last_way_start_idx integer := {$result[2]};
			last_way_end bigint;
		BEGIN
			CREATE TEMP TABLE _tmp_ways ( id bigint );
			CREATE TEMP TABLE _tmp_linestring ( seq_id integer, geom geometry(Point,4326) );

			SELECT w.nodes[array_upper(w.nodes, 1)]
			INTO last_way_end FROM ways w
			WHERE w.id = last_way;

			LOOP
				INSERT INTO _tmp_ways SELECT last_way;

				INSERT INTO _tmp_linestring
				SELECT i * 2000 + wn.sequence_id, n.geom
				FROM way_nodes wn
				JOIN nodes n ON (n.id = wn.node_id)
				WHERE wn.way_id = last_way
				AND wn.sequence_id >= last_way_start_idx;

				SELECT wn.way_id, wn.sequence_id + 1, w.nodes[array_upper(w.nodes, 1)]
				INTO last_way, last_way_start_idx, last_way_end
				FROM way_nodes wn
				JOIN ways w ON (w.id = wn.way_id)
				WHERE wn.node_id = last_way_end
				AND NOT EXISTS ( SELECT NULL FROM _tmp_ways tmp WHERE tmp.id = wn.way_id )
				AND w.tags -> 'waterway' IN ('stream', 'river', 'ditch', 'canal', 'wadi', 'drain')
				LIMIT 1;

				IF NOT FOUND THEN
					EXIT;
				END IF;

				i := i + 1;
			END LOOP;
		END$$;
	");

	$result = $db->query("
		SELECT ST_X(geom) AS lng, ST_Y(geom) AS lat
		FROM _tmp_linestring
		ORDER BY seq_id");

	$db->commit();

	$result->setFetchMode(PDO::FETCH_ASSOC);
	$first = true;
	foreach ($result as $row) {
		if ($first) {
			$first = false;
		} else {
			echo ',';
		}
		echo "[{$row['lat']},{$row['lng']}]";
	}
}

echo ']}';
