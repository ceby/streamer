import bobo
import postgresql

db = postgresql.open("pq://streamer@localhost/streamer")

node_from_id = db.prepare("""
SELECT wn.node_id, w.id AS way_id, wn.sequence_id FROM way_nodes wn
JOIN ways w ON (w.id = wn.way_id)
WHERE wn.node_id = $1::bigint
LIMIT 1""");

node_from_latlon = db.prepare("""
SELECT n.id AS node_id, w.id AS way_id, wn.sequence_id FROM nodes n
JOIN way_nodes wn ON (n.id = wn.node_id)
JOIN ways w ON (w.id = wn.way_id)
ORDER BY n.geom <-> ST_SetSRID(ST_Point($2::float, $1::float), 4326)
LIMIT 1""");

db.execute('CREATE TEMP TABLE _tmp_ways ( id bigint )')
db.execute('CREATE TEMP TABLE _tmp_linestring ( seq_id integer, geom geometry(Point,4326) )')

trace_stream_loop = db.execute("""
CREATE FUNCTION pg_temp.trace_stream_loop(last_way bigint, last_way_start_idx integer)
RETURNS void
AS $$DECLARE
	i integer := 1;
	last_way bigint := $1::bigint;
	last_way_start_idx integer := $2::bigint;
	last_way_end bigint;
BEGIN
	TRUNCATE TABLE _tmp_ways;
	TRUNCATE TABLE _tmp_linestring;

	SELECT w.nodes[array_upper(w.nodes, 1)]
	INTO last_way_end FROM ways w
	WHERE w.id = last_way;

	LOOP
		INSERT INTO _tmp_ways SELECT last_way;

		INSERT INTO _tmp_linestring
		SELECT i * 2000 + wn.sequence_id, n.geom
		FROM way_nodes wn
		JOIN nodes n ON (n.id = wn.node_id)
		WHERE wn.way_id = last_way
		AND wn.sequence_id >= last_way_start_idx;

		SELECT wn.way_id, wn.sequence_id + 1, w.nodes[array_upper(w.nodes, 1)]
		INTO last_way, last_way_start_idx, last_way_end
		FROM way_nodes wn
		JOIN ways w ON (w.id = wn.way_id)
		WHERE wn.node_id = last_way_end
		AND NOT EXISTS ( SELECT NULL FROM _tmp_ways tmp WHERE tmp.id = wn.way_id )
		LIMIT 1;

		IF NOT FOUND THEN
			EXIT;
		END IF;

		i := i + 1;
	END LOOP;
END$$ LANGUAGE plpgsql;
""");
trace_stream_loop = db.proc('pg_temp.trace_stream_loop(bigint,integer)')

trace_stream_fetch = db.prepare("""
SELECT ST_Y(geom) AS lng, ST_X(geom) AS lat
FROM _tmp_linestring
ORDER BY seq_id""")

@bobo.query('/')
def index():
	return open('index.html').read()

@bobo.query('/stream/:id', content_type='application/json')
def trace_id(id):
	return trace_stream(node_from_id(int(id)))

@bobo.query('/stream/:lat/:lon', content_type='application/json')
def trace_coords(lat, lon):
	return trace_stream(node_from_latlon(float(lat), float(lon)))

def trace_stream(cursor):
	response = { 'status': 'error' }
	if cursor:
		row = cursor[0]
		trace_stream_loop(row[1], row[2])
		result = trace_stream_fetch()
		if result:
			response['status'] = 'ok'
			response['id'] = row[0]
			response['linestring'] = result

	return response
