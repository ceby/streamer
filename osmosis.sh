#!/bin/sh
export PGPASSFILE=pgpass

if [[ $1 = 'import' ]]; then
	echo 'Importing from pbf'
	osmosis --rb file=texas-latest.osm.pbf --wp authFile=db.auth
	psql -U streamer streamer -c 'TRUNCATE relations; TRUNCATE relation_members'
	vacuumdb -z -U streamer streamer
	rm -f configuration.txt
	osmosis --rrii
	cat > configuration.txt <<END
baseUrl=http://download.geofabrik.de/north-america/us/texas-updates
maxInterval = 0
END
	echo "Don't forget to download state.txt!"
else
	osmosis --rri --wpc authFile=db.auth
	psql -U streamer streamer -c 'TRUNCATE relations; TRUNCATE relation_members'
	vacuumdb -z -U streamer streamer
fi
